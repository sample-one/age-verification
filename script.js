function checkAge() {
  let ageInput = document.getElementById('ageInput');
  let age = parseInt(ageInput.value);

  if (isNaN(age) || age < 0) {
      alert('Please enter a valid age.');
  } else {
      let resultElement = document.getElementById('result');
      if (age >= 18) {
          resultElement.innerText = 'You are 18 years or older. Welcome to the OTT platform!';
      } else {
          resultElement.innerText = 'You are under 18 years old. Access to the OTT platform is restricted.';
      }
  }
}
